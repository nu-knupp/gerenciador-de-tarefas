import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Gerenciadortarefas from './gerenciador';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <Gerenciadortarefas />,
  document.getElementById('root')
);

reportWebVitals();