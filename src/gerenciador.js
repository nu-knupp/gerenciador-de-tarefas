import React from 'react';
import './gerenciador.css';
import { useRoutes } from 'hookrouter';
import ListarTarefas from './listar-tarefas/listar-tarefas';
import CadastrarTarefa from './cadastrar/cadastrar-tarefa';
import AtualizarTarefa from './atualizar/atualizar-tarefa';

const routes = {
  '/': () => <ListarTarefas />,
  '/cadastrar': () => <CadastrarTarefa />,
  '/atualizar/:id': ({id}) => <AtualizarTarefa id={id} /> 
};

function GerenciadorTarefas() {
  return useRoutes(routes);
};

export default GerenciadorTarefas;
